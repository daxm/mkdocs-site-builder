# How to use this Repo
1. Get the latest version of the repo:
    * git clone <blah>
1. Use or Build Python Virtual Environment:
    * source ./venv/bin/activate
    * pip install -r requirements.txt
1. Run site for development:
    * ./run_docker_container.sh
    * Runs a docker container named 'mkdocs-site' on port 8000 (http://localhost:8000)
1. Build static content:
    * ./build_static_site.sh
    * copy/move contents of ./mkdocs/site to webserver.
 