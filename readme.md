# How to use this Repo

## Setting up the repo
1. Get this repo:
    * `git clone <blah>`
    * `cd <blah>`

## Set up mkdocs environment
1. Modify **./mkdocs/mkdocs.yml**:
    * Update **site_name:** to desired title of your documentation project.
    * Update **site_url:** to desired FQDN.
    * (Optional) Select desired **theme:** by commenting out one of the options.
    * Adjust **nav:** to desired layout of the left pane navigation.  (You'll update this over time as your site 
      grows.)

## Set up the Docker environment
1. Edit `docker-compose.yml` file.
   1. Change `container_name` variable to something meaningful.
   2. By default `ports` will forward external TCP/8000 to TCP/8000 in the container.  If you have other things running
on TCP/8000 (like another instance of this container build) then change the external listening port to something else,
say, TCP/8001.  (e.g. `8001:8000`)

## Run Docker container
1. Run site:
    * Run script `./run_docker_container.sh`.  By default, runs a docker container named 'mkdocs-site' on port 8000 
      (http://localhost:8000)
    * Use `docker stop <container_name>` to kill Docker container when done.

## Develop website
1.  Put your MarkDown files in the **./mkdocs/docs** directory.
1.  Put your images in the **./mkdocs/docs/img** directory.
1.  Update the **./mkdocs/mkdocs.yml** file's **nav:** section to suit your site.

## Common git commands:
* `git status`:  Simple report explaining the state of this repository.
* `git add <blah>`:  Adds new/modified/deleted files to the git repository.
* `git commit -m "<message>"`:  Commit added files.
* `git push`: Push committed files up to gitlab.
